/* 
 * The MIT License
 *
 * Copyright 2017 Fonk, Danny <danny.fonk@gmx.de>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package de.dfonk.gameoflife;

import java.time.Instant;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.layout.GridPane;

/**
 * A GameGrid is a two-dimensional grid that contains {@link Cells}. It also
 * contains the logic according to the game and offers all necessary methods
 * like starting the game, stopping the game, updating the grid et cetera.
 *
 * @author Fonk, Danny <danny.fonk@gmx.de>
 */
public class GameGrid extends GridPane {

    static boolean running;
    static int iteration = 0;
    static double timeout;

    /**
     * Starts the game and while there are changes in the {@link GameGrid}
     * (based on the Cell-state) the next iteration is executed.
     */
    public void startGame() {

        setIteration(0);

        while (GameGrid.running) {

            // Make our iteration thread-safe
            Platform.runLater(() -> {
                iterate();
                MainApp.controller.setTime(Instant.now());
            });

            // This manages our speed
            try {
                Thread.sleep((int) (timeout * 100));
            } catch (InterruptedException ex) {
                Logger.getLogger(GameGrid.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    /**
     * Logic for a single turn.
     */
    private void iterate() {

        int[][] currGen = gameGridToArray();

        boolean switched = false;

        // set the new generation
        for (Node child : this.getChildren()) {
            int row = GridPane.getRowIndex(child);
            int col = GridPane.getColumnIndex(child);
            int numOfNeighbors = calcNumberOfNeighbors(currGen, col, row);
            Cell cell = (Cell) child;
            if (cell.calculateNextState(numOfNeighbors)) {
                switched = true;
            }
        }

        // Increase the iteration number
        setIteration(GameGrid.iteration + 1);

        // In case we didnt switch a state of a cell once, there is no
        // movement in the cells anymore. Stop the iteration.
        if (!switched) {
            MainApp.controller.stopGame();
        }
    }

    /**
     * Reset the game. The field itself is untouched but all cells will be reset
     * to be "dead" and the iteration-count is set to 0.
     */
    public void resetGame() {
        // Clear the field
        this.getChildren().clear();
        setIteration(0);
    }

    /**
     * Draws the shape upon the GameGrid.
     *
     * @param shape
     */
    public void draw(int[][] shape) {

        resetGame();

        // Actually every array inserted here is a rectangle but 
        // still fetch the case that the inserted array ain't. So we
        // define the number of columns by searching for the largest
        // number.
        int cols = 0;
        int rows = shape.length;
        for (int[] array : shape) {
            if (array.length > cols) {
                cols = array.length;
            }
        }

        MainApp.controller.updateInputValues(rows, cols);

        initField(cols, rows);

        // Set initial selection of cells and their state
        for (int shapeRows = 0; shapeRows < shape.length; shapeRows++) {
            for (int shapeCols = 0; shapeCols < shape[shapeRows].length; shapeCols++) {
                for (Node n : this.getChildren()) {
                    int row = GridPane.getRowIndex(n);
                    int col = GridPane.getColumnIndex(n);
                    if (row == shapeRows && col == shapeCols) {
                        if ((shape[shapeRows][shapeCols] == 1)) {
                            ((Cell) n).switchState();
                        }
                    }
                }
            }
        }
    }

    /**
     * Used to initialize the field upon start or resetting the game.
     *
     * @param width
     * @param height
     */
    public void initField(int width, int height) {
        for (int i = 0; i < width; i++) {
            // Columns
            for (int j = 0; j < height; j++) {
                // Rows
                Cell cell = new Cell(false);
                this.add(cell, i, j);
            }
        }
    }

    /**
     * Updates the field by adding or removing columns/rows on the
     * {@link GameGrid}. The state of cells which are not effected will remain.
     *
     * @param width new width of {@link GameGrid}
     * @param height new height of {@link GameGrid}
     */
    public void updateField(int width, int height) {
        if (getRowCount() != height) {
            if (getRowCount() > height) {
                // Remove rows until we match the desired height
                ObservableList<Node> children = this.getChildren();
                children.removeIf(x -> GridPane.getRowIndex(x) >= height);
            } else {
                // Add rows until we match the desired height
                do {
                    int rowNumber = getRowCount();
                    for (int i = 0; i < getColCount(); i++) {
                        Cell cell = new Cell(false);
                        this.add(cell, i, rowNumber);
                    }
                } while (getRowCount() < height);
            }
        }

        if (getColCount() != width) {
            if (getColCount() > width) {
                // Remove columns until we match the desired width
                // Remove rows until we match the desired height
                ObservableList<Node> children = this.getChildren();
                children.removeIf(x -> GridPane.getColumnIndex(x) >= width);
            } else {
                // Add columns until we match the desired width
                do {
                    int nextColumn = getColCount();
                    for (int i = 0; i < getRowCount(); i++) {
                        Cell cell = new Cell(false);
                        this.add(cell, nextColumn, i);
                    }
                } while (getColCount() < width);
            }
        }

        // Update the GUI and set a new size in case the size has been altered
        MainApp.stage.sizeToScene();
    }

    /**
     * Will calculate and return the number of living neighbors for a given
     * cell-index.
     *
     * @param grid the current state of the {@link GameGrid}
     * @param column column-index of the cell the calculation shall be applied
     * for
     * @param row row-index of the cell the calculation shall be applied for
     * @return the number of living neighbors
     */
    private int calcNumberOfNeighbors(int[][] grid, int column, int row) {

        // init necessary vars
        int n1, n2, n3, n4, n5, n6, n7, n8;
        int rowBefore, rowNext, colBefore, colNext;

        // Consider special cases where the gamegrid applies over the edge
        if (column == 0) {
            // Our cell is in the first column, so our neighbors
            // are located in the opposite column on the right.
            colBefore = grid.length - 1;
            colNext = column + 1;
        } else if (column == grid.length - 1) {
            // Our cell is in the last column, so our neighbors
            // are located in the opposite column on the left.
            colBefore = column - 1;
            colNext = 0;
        } else {
            // Our cell is nor in the first or in the last column. Considering 
            // this we don't have a special case and we can access the 
            // (col)neighbors without any fear of an IndexOutOfBounds-Exception.
            colNext = column + 1;
            colBefore = column - 1;
        }

        if (row == 0) {
            // Our cell is in the first row, so our neighbors
            // are located in the opposite row on the bottom of the gamegrid.
            rowBefore = grid[0].length - 1;
            rowNext = row + 1;
        } else if (row == grid[0].length - 1) {
            // Our cell is in the last row, so our neighbors
            // are located in the opposite row on the top of the gamegrid.
            rowNext = 0;
            rowBefore = row - 1;
        } else {
            // Our cell is nor in the first or in the last row. Considering 
            // this we don't have a special case and we can access the 
            // (row)neighbors without any fear of an IndexOutOfBounds-Exception.
            rowNext = row + 1;
            rowBefore = row - 1;
        }

        // These are the neighbors for the given cell
        // Their value is either 1 if alive or 0 if dead
        // We can simple count the living neighbors by
        // totaling the values
        n1 = grid[colBefore][rowBefore];
        n2 = grid[colBefore][row];
        n3 = grid[colBefore][rowNext];
        n4 = grid[column][rowBefore];
        n5 = grid[column][rowNext];
        n6 = grid[colNext][rowBefore];
        n7 = grid[colNext][row];
        n8 = grid[colNext][rowNext];

        int countLivingNeighbors = n1 + n2 + n3 + n4 + n5 + n6 + n7 + n8;
        return countLivingNeighbors;
    }

    /**
     * Returns the number of rows for the current {@link GameGrid}.
     *
     * @return
     */
    private int getRowCount() {
        int numRows = this.getRowConstraints().size();
        for (int i = 0; i < this.getChildren().size(); i++) {
            Node child = this.getChildren().get(i);
            if (child.isManaged()) {
                Integer rowIndex = GridPane.getRowIndex(child);
                if (rowIndex != null) {
                    numRows = Math.max(numRows, rowIndex + 1);
                }
            }
        }
        return numRows;
    }

    /**
     * Returns the number of columns for the current {@link GameGrid}.
     *
     * @return
     */
    private int getColCount() {
        int numCols = this.getColumnConstraints().size();
        for (int i = 0; i < this.getChildren().size(); i++) {
            Node child = this.getChildren().get(i);
            if (child.isManaged()) {
                Integer colIndex = GridPane.getColumnIndex(child);
                if (colIndex != null) {
                    numCols = Math.max(numCols, colIndex + 1);
                }
            }
        }
        return numCols;
    }

    /**
     * Stores the current {@link GameGrid} into a two-dimensional array for
     * later use.
     *
     * @return
     */
    public int[][] gameGridToArray() {
        int[][] currGen = new int[getColCount()][getRowCount()];

        // Save the current generation into a two-dimensional array
        this.getChildren().stream().forEach((child) -> {
            int row = GridPane.getRowIndex(child);
            int col = GridPane.getColumnIndex(child);
            currGen[col][row] = ((Cell) child).alive ? 1 : 0;
        });
        return currGen;
    }

    public void setRunning(boolean running) {
        GameGrid.running = running;
    }

    public static boolean isRunning() {
        return running;
    }

    public static void setTimeout(double timeout) {
        GameGrid.timeout = timeout;
    }

    public static void setIteration(int iteration) {
        GameGrid.iteration = iteration;
        MainApp.controller.setIterationLblText(iteration + "");
    }

}

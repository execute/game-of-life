/* 
 * The MIT License
 *
 * Copyright 2017 Fonk, Danny <danny.fonk@gmx.de>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package de.dfonk.gameoflife;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.scene.control.Button;

/**
 * This class describes a cell on the {@link GameGrid}. It got different states
 * and depending on his state, it will add different CSS-classes. Other than
 * that, it doesn't contain any real intelligence. Every cell is clickable and
 * will switch it's state upon clicking.
 *
 * @author Fonk, Danny <danny.fonk@gmx.de>
 */
public class Cell extends Button {

    boolean alive;

    public Cell(boolean alive) {
        this.alive = alive;
        super.getStyleClass().add("cell");
        super.setOnAction((ActionEvent event) -> {
            switchState();
        });
    }

    /**
     * Switches the state of a cell. In case it's alive, it will be dead and
     * vice versa. Switching will add or remove different Style-Classes in order
     * to display the cell state in GUI.
     */
    public void switchState() {
        if (isAlive()) {
            // Cell is alive right now. We set it to dead.
            // Remove all possible style-class that could have been applied.
            super.getStyleClass().remove("alive");
            super.getStyleClass().remove("survived");
            super.getStyleClass().remove("static");
            setAlive(false);
        } else {
            // Cell is dead right now, resurrect it.
            super.getStyleClass().add("alive");
            setAlive(true);
        }
    }

    /**
     * The next state is calculated by the number of their living neighbors. The
     * rules are as followed:
     * <ol>
     * <li> Every living cell with less than 2 living neighbor-cells, dies.
     * <li> Every living cell with exact 2 or 3 living neighbor-cells, lives on.
     * <li> Every living cell with more then 3 living neighbor-cells, dies.
     * <li> Every dead cell with exactly 3 living neighbors is resurrected.
     * </ol>
     *
     * The idea to dye the cell based on their status and/or living-duration is
     * inspired by http://conwaylife.appspot.com/pattern/acorn.
     *
     * @param numberOfNeighbors
     * @return a boolean indicating if the cell has changed or not
     */
    public boolean calculateNextState(int numberOfNeighbors) {
        if (isAlive()) {
            if (numberOfNeighbors < 2) {
                // 1) Every living cell with less than 2 living neighbor-cells, dies.
                switchState();
                return true;
            } else if (numberOfNeighbors <= 3) {
                // 2) Every living cell with exact 2 or 3 living neighbor-cells, lives on.

                // Usually we don't need to do anything at this point but its
                // interesting to see which cells lasted longer already so we
                // mark cells who are living for more than one generation and 
                // those who are living since 2 generations
                if (!this.getStyleClass().contains("survived") && !this.getStyleClass().contains("static")) {
                    // It is alive for two generations already. Mark it as static.
                    this.getStyleClass().add("survived");
                } else if (this.getStyleClass().contains("survived")) {
                    this.getStyleClass().remove("survived");
                    this.getStyleClass().add("static");
                }
            } else {
                // 3) Every living cell with more then 3 living neighbor-cells, dies.
                switchState();
                return true;
            }
        } else if (numberOfNeighbors == 3) {
            // 4) Every dead cell with exactly 3 living neighbors is resurrected.
            switchState();
            return true;
        }

        return false;
    }

    public boolean isAlive() {
        return alive;
    }

    private void setAlive(boolean alive) {
        this.alive = alive;
    }
}

/* 
 * The MIT License
 *
 * Copyright 2017 Fonk, Danny <danny.fonk@gmx.de>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package de.dfonk.gameoflife;

import de.dfonk.gameoflife.presets.Beacon;
import de.dfonk.gameoflife.presets.Beehive;
import de.dfonk.gameoflife.presets.Blinker;
import de.dfonk.gameoflife.presets.Block;
import de.dfonk.gameoflife.presets.Boat;
import de.dfonk.gameoflife.presets.Glider;
import de.dfonk.gameoflife.presets.LightweightSpaceship;
import de.dfonk.gameoflife.presets.Loaf;
import de.dfonk.gameoflife.presets.Pentadecathlon;
import de.dfonk.gameoflife.presets.Preset;
import de.dfonk.gameoflife.presets.Pulsar;
import de.dfonk.gameoflife.presets.Toad;
import de.dfonk.gameoflife.presets.Tub;
import java.net.URL;
import java.time.Instant;
import java.util.ResourceBundle;
import javafx.animation.PauseTransition;
import javafx.beans.value.ObservableValue;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.Slider;
import java.time.Duration;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import javafx.application.Platform;

/**
 * This class describes the GUI and its behaviour.
 *
 * @author Fonk, Danny <danny.fonk@kdo.de>
 */
public class FXMLController implements Initializable {

    @FXML
    private GameGrid playingField;

    @FXML
    private Button startBtn;

    @FXML
    private Button stopBtn;

    @FXML
    private Button resetBtn;

    @FXML
    private NumberTextField inputX;

    @FXML
    private NumberTextField inputY;

    @FXML
    private Slider speedSlider;

    @FXML
    private Label iterationLbl;

    @FXML
    private Label time;

    private Instant executionTime;

    @FXML
    private void startGame(ActionEvent event) {

        executionTime = Instant.now();

        Task<Void> task = new Task<Void>() {
            @Override
            protected Void call() throws Exception {
                playingField.setRunning(true);
                playingField.startGame();
                return null;
            }
        };

        // Disable controls
        startBtn.setDisable(true);
        resetBtn.setDisable(true);
        stopBtn.setDisable(false);
        new Thread(task).start();
    }

    @FXML
    private void handlePresetSelection(ActionEvent event) {
        MenuItem mItem = (MenuItem) event.getSource();
        Preset preset = null;
        switch (mItem.getText()) {
            case "Block":
                preset = new Block();
                break;
            case "Beehive":
                preset = new Beehive();
                break;
            case "Loaf":
                preset = new Loaf();
                break;
            case "Boat":
                preset = new Boat();
                break;
            case "Tub":
                preset = new Tub();
                break;
            case "Blinker (period 2)":
                preset = new Blinker();
                break;
            case "Toad (period 2)":
                preset = new Toad();
                break;
            case "Beacon (period 2)":
                preset = new Beacon();
                break;
            case "Pulsar (period 3)":
                preset = new Pulsar();
                break;
            case "Pentadecathlon (period 15)":
                preset = new Pentadecathlon();
                break;
            case "Glider":
                preset = new Glider();
                break;
            case "Lightweight spaceship (LWSS)":
                preset = new LightweightSpaceship();
                break;
        }
        if (preset != null) {
            preset.draw();
        }
    }

    @FXML
    private void handleStopButton(ActionEvent event) {
        this.stopGame();
    }

    @FXML
    private void resetGameField(ActionEvent event) {
        playingField.resetGame();
        playingField.initField(inputX.getValue(), inputY.getValue());
    }

    @FXML
    private void loadGame(ActionEvent event) {
        GameFileHelper gfHelper = new GameFileHelper();
        gfHelper.loadGame();
    }

    @FXML
    private void saveGame(ActionEvent event) {
        GameFileHelper gfHelper = new GameFileHelper();
        gfHelper.saveGame(playingField.gameGridToArray());
    }

    public void draw(int[][] shape) {
        playingField.draw(shape);
    }

    /**
     * Defines the timeout between the iterations. A low timeout results in a
     * fast pace while a high timeout results in slow iterations.
     *
     * @param event
     */
    @FXML
    private void handleTimeOutChange(Event event) {
        setTimeout();
    }

    @FXML
    private void exit(ActionEvent event) {
        System.exit(0);
    }

    private void setTimeout() {
        // We should invert the slider, since users are used to low values (left)
        // will equal to low speed while they'd expect a speed-increase while
        // sliding to the right. The formula for this is quiet simple:
        // Max - (Value - Min) = Speed_inverted
        double speed = speedSlider.getMax() - (speedSlider.getValue() - speedSlider.getMin());
        GameGrid.setTimeout(speed);
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {

        // Initialize the playing field
        playingField.initField(inputX.getValue(), inputY.getValue());
        setTimeout();

        // Add custom listeners to the input-fields for width and height. We add
        // a pause to these listeners, so it won't fire on every single number.
        // Instead it waits one second and in case another input is made, the
        // timer will reset. After the timer expired, it will apply the changes
        // to the playing-field.
        final PauseTransition pause = new PauseTransition(javafx.util.Duration.seconds(1));
        inputX.textProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            pause.playFromStart();
            pause.setOnFinished((finished) -> {
                try {
                    playingField.updateField(inputX.getValue(), inputY.getValue());
                } catch (NumberFormatException ex) {
                    // This can happen in case we deleted the whole input-String
                    // So we just fetch that case and do nothing with it. Instead
                    // we just wait for another input
                }
            });
        });

        inputY.textProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            pause.playFromStart();
            pause.setOnFinished((finished) -> {
                try {
                    playingField.updateField(inputX.getValue(), inputY.getValue());
                } catch (NumberFormatException ex) {
                    // This can happen in case we deleted the whole input-String
                    // So we just fetch that case and do nothing with it. Instead
                    // we just wait for another input
                }
            });
        });
    }

    public void updateInputValues(int height, int width) {
        inputY.setText(String.valueOf(height));
        inputX.setText(String.valueOf(width));
    }

    public void setIterationLblText(String text) {
        this.iterationLbl.setText(text);
    }

    public void setTime(Instant time) {
        Duration d = Duration.between(executionTime, time);
        String timeString = LocalTime.MIDNIGHT.plus(d).format(DateTimeFormatter.ofPattern("mm:ss.SSS"));
        this.time.setText(timeString);
    }

    /**
     * Stops the game and activates the controls to start a new game.
     */
    public void stopGame() {
        playingField.setRunning(false);
        startBtn.setDisable(false);
        resetBtn.setDisable(false);
        stopBtn.setDisable(true);
    }
}

/* 
 * The MIT License
 *
 * Copyright 2017 Fonk, Danny <danny.fonk@gmx.de>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package de.dfonk.gameoflife;

import javafx.scene.control.TextField;
import javafx.scene.control.TextFormatter;

/**
 * A {@link TextField} that holds only numbers. In case it doesn't match the
 * requirements, the value isn't changed.
 */
public class NumberTextField extends TextField {

    public NumberTextField() {
        addTextFormatter();
    }

    public NumberTextField(String text) {
        super(text);
        addTextFormatter();
    }

    private void addTextFormatter() {
        this.setTextFormatter(new TextFormatter<>(change -> {
            String text = change.getControlNewText();

            // In case the input is empty, we just return this in order to
            // allow an empty String as well. This will allow the user to delete
            // the current input before adding a new input.
            if (!text.equals("")) {
                // Allow any numbers except 0
                if (text.matches("[1-9]([0-9]*?)")) {
                    return change;
                }

                return null;
            } else {
                return change;
            }
        }));
    }

    public Integer getValue() throws NumberFormatException {
        return Integer.valueOf(this.getText());
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.dfonk.gameoflife;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.stage.FileChooser;

/**
 *
 * @author Fonk, Danny <danny.fonk@kdo.de>
 */
public class GameFileHelper {

    /**
     * Loads a new game-state from a file and displays it in the GUI.
     */
    public void loadGame() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Open Game File");
        File gameFile = fileChooser.showOpenDialog(MainApp.stage);
        BufferedReader reader = null;

        int row = 0;
        int col = 0;

        List<String> lines = new ArrayList<>();

        try {
            reader = new BufferedReader(new FileReader(gameFile));

            String line;
            while ((line = reader.readLine()) != null) {
                lines.add(line);
                String[] cols = line.split(","); //note that if you have used space as separator you have to split on " "
                for (String c : cols) {
                    col++;
                }
                row++;
            }

            // Calculate the number of columns again, since the iteration
            // sums the number up wrongly
            col = col / row;

            // create game-array
            int[][] game = new int[row][col];

            // fill the array
            int currRow = 0;
            for (String l : lines) {
                String[] cols = l.split(","); //note that if you have used space as separator you have to split on " "

                for (int currCol = 0; currCol < cols.length; currCol++) {
                    game[currRow][currCol] = Integer.valueOf(cols[currCol]);
                }
                currRow++;
            }

            // Load the array into the GUI
            MainApp.controller.draw(game);
        } catch (IOException ex) {

        } finally {
            try {
                reader.close();
            } catch (IOException ex) {
                Logger.getLogger(GameFileHelper.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    /**
     * Saves the current game-state to a file.
     *
     * @param game
     */
    public void saveGame(int[][] game) {
        BufferedWriter writer = null;

        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Save Game File");
        fileChooser.setInitialFileName("conways_game_of_life.txt");
        File file = fileChooser.showSaveDialog(MainApp.stage);

        StringBuilder builder = new StringBuilder();

        // Actually every array inserted here is a rectangle but 
        // still fetch the case that the inserted array ain't. So we
        // define the number of columns by searching for the largest
        // number.
        int cols = 0;
        int rows = game.length;
        for (int[] array : game) {
            if (array.length > cols) {
                cols = array.length;
            }
        }

        for (int i = 0; i < cols; i++) {
            for (int j = 0; j < rows; j++) {
                builder.append(game[j][i]);
                if (j < rows - 1) {
                    builder.append(",");
                } else {
                    builder.append(System.getProperty("line.separator"));
                }
            }
        }

        try {
            writer = new BufferedWriter(new FileWriter(file));
            writer.write(builder.toString());//save the string representation of the board
        } catch (IOException ex) {
            Logger.getLogger(GameFileHelper.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                writer.close();
            } catch (IOException | NullPointerException ex) {
                Logger.getLogger(GameFileHelper.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
